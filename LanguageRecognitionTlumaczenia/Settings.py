TEST_DIR = './test/' # folder z danymi testowymi
TRAINING_DIR = './training' # folder z danymi treningowymi
MAX_ERROR = 0.5 # przyjęty maksymalny błąd, jest to moment, w którym zakańczamy trenowanie dla danego języka
NUMBER_OF_LETTERS = 26 # tyle liter obsługujemy - normalny, czysty rasowo alfabet
LEARNING_PARAMETER = 15 # alfa
