import os

import Settings


class FileReader:
    def __init__(self, path):
        self.file = path

    # tutaj robimy statystyki z literek
    def get_letter_values(self):
        letters = [0] * Settings.NUMBER_OF_LETTERS # tworzymy array o wymiarze takim, jak ilość liter
        txt_file = open(self.file)
        letter_count = 0
        for line in txt_file: # lecimy po linijce...
            for letter in line: # ... a tu po literce
                letter = letter.lower() # niech wszystkie znaki będą małymi literami
                letter_no = ord(letter) - 97
                if 0 <= letter_no <= 25:  # specjalne znaki lecą w pizdu (patrzymy po kodach ASCII)
                    letters[letter_no] += 1
                    letter_count += 1

        if letter_count == 0:
            print("ERROR: Empty file given as a training set!")
        else:
            for i in range(len(letters)):
                letters[i] /= letter_count # liczmy średnią arytmetyczną (np w 'aaab' przy 'b' będzie 1/4)

        return letters

    # bierzemy nazwę z języka patrząc na jego pierwsze 2 litery w nazwie pliku
    def get_language_name(self):
        return os.path.basename(self.file)[:2]

