import glob
import os

import Settings
from FileReader import FileReader
from Perceptron import Perceptron


class Layer:
    def __init__(self):
        self.perceptrons = []
        languages = self.detect_languages()
        self.create_perceptrons(languages)
        self.train_perceptrons()

    # sprawdzamy jakie języki mamy do nauki patrząc na to, jakie są nazwy katalogów
    def detect_languages(self):
        return [d for d in os.listdir(Settings.TRAINING_DIR)]

    def create_perceptrons(self, languages): # robimy perceptron dla każdego języka
        for lang in languages:
            self.perceptrons.append(
                Perceptron(os.path.basename(lang))
            )

    def train_perceptrons(self):
        # lecimy po plikach w folderze, * to losowy znak dla pythona
        files = glob.glob(Settings.TRAINING_DIR + '/**/*.txt', recursive=True)
        repeat = 1 # żeby wgl wejść do pętli
        while repeat == 1:
            repeat = 0

            for f in files:
                error = 0
                reader = FileReader(f) # korzystamy z zewnętrznej klasy która ułatwi nam czytanie plików
                letter_values = reader.get_letter_values() # mamy już statystyki dla liter
                lang = reader.get_language_name() # bierzemy nazwę języka dla tych statystyk literek

                for p in self.perceptrons:
                    output = p.train(letter_values, lang) # trenujemy
                    error += 0.5 * (p.desired_output(lang) - output) ** 2 # wyliczamy błąd
            # warunek wyjścia z pętli while; uczymy do momentu, w którym wyniki będą mniejsze od ustalonego błędu
            if error > Settings.MAX_ERROR:
                repeat = 1


    def test_output(self, input_file): # dajemy output i widzimy, który perceptron daje wartość najbliżej 1
        reader = FileReader(input_file)
        letter_values = reader.get_letter_values()

        values = {}
        for p in self.perceptrons:
            values[p.name] = p.output(letter_values)

        print(values)

        return max(values, key=values.get) # jako wynik zwracamy język, który był najbliżej 1

